openbsdver := $(shell cat version.txt)

dist:
	./makesrc
	./signtarballs
	rm -r bld xenocara-$(openbsdver).tar.gz

upload:
	rsync -av -e 'ssh -p 51000' --progress src/xenocara-libre/$(openbsdver) repo@dusseldorf.hyperbola.info:/srv/repo/sources/xenocara-libre

.PHONY: dist upload
